# fastlane-ios

<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->


<!-- PROJECT LOGO -->



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>How to Use Fastlan</summary>
  <ol>
    <li>
      <a href="#about-the-fastlane">Fastlane คืออะไร ?</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#install-fastlane-on-mac">ติดตั้ง fastlane ในเครื่อง</a></li>
        <li><a href="#install-fastlane-on-project">ติดตั้ง fastlane ในโปรเจค</a></li>
      </ul>
    </li>
    <li><a href="#fastfile">How to Use Fastfile</a>
     <ul>
        <li><a href="#build-app">Build App to ipa</a></li>
      </ul>
    </li>
    <li><a href="#plugin ">Plugin</a>
     <ul>
        <li><a href="#firebase-app-distribution">Firebase App Distribution</a></li>
         <li><a href="#upload-symbols-to-crashlytics">Upload Symbols to Crashlytics</a></li>
      </ul>
    </li>
    <li><a href="#run-multiple-lanes">คำสั่งใช้หลายๆ Lane พร้อมกัน</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Fastlane

![Product Name Screen Shot][images-fastlane]

เป็นเครื่องมือที่ช่วยให้เราทำ Run Test, Build, Deploy เป็นเรื่องง่ายๆ

ทำไมเราถึงต้องใช้ ?: :smile:
* Deliver คือ เผยแพร่ Beta App สำหรับแอป iOS และ Android โดยอัตโนมัติ
* Pem คือการ สร้างและต่ออายุโปรไฟล์การแจ้งเตือน (push notification profile)
* Produce คือการสร้างแอพ iOS ใหม่บน App Store Connect
* Snapshot คือการ screenshot รูปของ App เช่น iPhone ภาษไทย, iPhone ภาษาอังกฤษ 
* Sign คือการ create, renew, download, repair provisioning profiles (ด้วย command). เช่น App Store, Ad Hoc, Development and Enterprise profiles
* Cert คือการ create an iOS code signing certificate and then a provisioning profile for your app
* Gym คือการ build app
* Scan คือการ run test
* Frameit คือการ เตรียมภาพหน้าจอบนเครื่องจริงสำหรับ App Store 


<!-- GETTING STARTED -->
## Getting Started

### Install Fastlane on Mac

System Ruby + RubyGems (macOS/Linux/Windows) 
```sh
sudo gem install fastlane
```
Or Homebrew (macOS)
```sh
brew install fastlane
```

### Install Fastlane on Project
1. หลังจากติดตั้งเสร็จแล้วให้เข้าไปใน directory ของ project
```sh
cd {{directory Project}}
```
2. ติดตั้ง fastlane ในโปรเจค (ถ้ามีคำถามว่าเราใช้ fastlane ทำอะไร ให้เลือก 4 เป็น Manual ไปก่อน..)
```sh
fastlane init
```
![Product Name Screen Shot][images-install-fastlane]

เสร็จแล้วในโปรเจคเราจะมี Folder ขึ้นมา 
ซึ่งจะประกอบไปด้วย 3 ไฟล์
* Appfile : setting app ของเรา ซึ่งไม่ต้องใช้ก็ได้
* Fastfile : เราจะใช้ไฟล์นี้เพื่อ setting สิ่งต่างๆ เพื่อทำ CI/CD กันนะ
* Pluginfile : ไฟล์นี้จะรวบรวม plugin ที่เราได้ติดตั้งเข้าไปในโปรเจค 
(จะมีไฟล์นี้ก็ต่อเมื่อเราลง plugin เพิ่มนะ)
สามารถไปดู Plugin ต่างได้ที่นี่ ว่าเราสามารถใช้อะไรได้บ้่าง http://docs.fastlane.tools/plugins/available-plugins/

![Product Name Screen Shot][images-install-fastlane-file]
<!-- USAGE EXAMPLES -->
## Fastfile
### Build App
คำสั่ง gym คือการ Build app ลืมรึยัง ?
```sh
  desc "Build app (Test) and export ipa" // รายละเอียดของ lane นี้ว่าต้องการทำอะไร
  lane :build_test do // ชื่อ Lane
    gym( //ใช้คำสั่งนี้เพื่อ Build app เป็น .ipa
      scheme: "AutomateDeploy", //ชื่อ Scheme ในโปรเจคของเรานั่นเอง
      export_options: { // 
        method: "development", //ชนิดของ Cer sign เช่น app-store, ad-hoc
        provisioningProfiles: {
          "com.xxx.xxx" => "ชื่อของ Provisioning Profiles"
        }
      },
      output_directory: "./artifact", //เราต้องให้ไฟล์ ipa ออกไปที่ folder ไหน
      output_name: "automateDeploy.ipa" // ตั้งชื่อไฟล์ตามใจเลยจ้า
    )
  end
```
ซึ่งเราสามารถใช้ Parameter อะไรภายใต้ gym ได้บ้างให้เข้าไปดูได้ที่ลื่งค์นี้เลย https://docs.fastlane.tools/actions/gym/

พิมพ์ Script ของ gym เสร็จแล้วกด Save แล้วลอง Run คำสั่งกันนน

```sh
fastlane build_test // build_test คือขื่อ Lane ของด้านบน
```

ก็จะได้ folder ออกมาเช่นนี้ 

![Product Name Screen Shot][images-gym-build]

เราจะได้ไฟล์มา 2 ไฟล์จากการใช้คำสั่งของ gym ด้านบน 
* iPA File
* dsym File 
// สำหรับคนที่เคยใช้ crashlytic น่าจะคุ้นเคยสำหรับคุ้นเคยการใช้ไฟล์นี้เพื่อดูการ Crash ภายในแอพของเรา แต่ถ้าไม่รู้จัดก็ข้ามไปก่อนก็ได้นะ




เราจะมาดู Fastfile กัน เราจะมายกตัวอย่าง Plugin ที่เราจะใช้ในวันนี้
## Plugin 

### firebase_app_distribution
สำหรับใครที่เคยคุ้นชิ้นกับการเอาแอพขึ้น Firebase อยู่แล้ว Plugin นี้จะน่าสนใจยิ่งขึ้น เพราะเราจะสามารถนำแอพขึ้น Firebase ได้ภายใน Script เดียวเลย

เริ่มจาก Add Plugin เข้ามาใน Project ก่อน

```sh
fastlane add_plugin firebase_app_distribution // fastlane add_plugin และตามด่้วยชื่อ plugin ได้เลยนะ
```
สามารถไปดู Plugin ต่างๆ ได้ที่นี่ ว่าเราสามารถใช้อะไรได้บ้่าง http://docs.fastlane.tools/plugins/available-plugins/

Add Plugin เสร็จเรียบร้อยเรามาเขียน Script ต่อใน Appfile ต่อกันเลยดีกว่า

```sh
default_platform(:ios)
FIREBASE_TOKEN = ENV['FIREBASE_TOKEN'] //ประกาศไว้ด้านบนสุดๆ ไปเลยจ้า
platform :ios do
  desc "Distribute app via Firebase (Test)"
  lane :distribute_test do
    firebase_app_distribution(
      app: "x.xxxx:idxxx", //ID APP เอามาจาก Project Setting ของ Firebase ที่สร้างขึ้นมา
      ipa_path: "./artifact/automateDeploy.ipa", // directory ipa ที่เราสร้างขึ้นหลังจากเราใช้คำสั่ง gym ด้านบน
      groups: "tester", //ชื่อกรุ๊ป
      release_notes: "Update v1.0.1 via Fastlane!!", //ใส่โน้ตอะไรก็ได้นะ
      firebase_cli_path: "/usr/local/bin/firebase", //บังคับใส่ของ filebase นะจ๊ะ
      firebase_cli_token: FIREBASE_TOKEN, //บังคับใส่ของ filebase นะจ๊ะ
    )
  end
```

เสร็จแล้ว ง่ายมะ ต่อไปเราก็จะ Distrubute ให้ User ได้เลย โดยใช้คำสั่งนี้

```sh
fastlane distribute_test //distribute_test ก็คือชื่อ lane ด้านบน ที่เขียนเมื่อกี้นั่นเอง
```

แต่ช้าก่อน.... มันจะ Error เอานะ เพราะเรายังไม่ได้ login firebase เลย ระบบจะรู้จัก firebase เราได้ยังไงกันนนน ????

พิมพ์คำสั่งนี้เพื่อ login firebase ก่อนนะจ๊ะ

```sh
firebase login
```
เสร็จแล้วจะระบบ auto open browser ขึ้นมาให้เราไป login ได้เลย หรือว่าเราจะ click link ที่ขึ้นมาแบบในรุป ก็ได้เหมือนกันน

![Product Name Screen Shot][images-firebase-login] 


ลองคำสั่งนี้อีกครั้งนะ น่าจะได้แล้วล่ะ

```sh
fastlane distribute_test //distribute_test ก็คือชื่อ lane ด้านบน ที่เขียนเมื่อกี้นั่นเอง
```

### upload_symbols_to_crashlytics
Plugin ตัวนี้เป็นตัวที่น่าสนใจอีก 1 ตัว เพราะจะช่วยให้เราสามารถอัพโหลดไฟล์ Dsym ขึ้นไปบน firebase crashlytic ได้ใน scipt เดียวเลยนะจ๊ะ

เริ่ม... Add Plugin upload_symbols_to_crashlytics เข้ามาในโปรเจคของเรา อย่าลืม cd.. ก่อนใช้คำนั่งนี้นะ 

```sh
fastlane add_plugin upload_symbols_to_crashlytics 
```

เขียน script ใน Appfile กัน 

```sh
  desc "Upload dsym app file via Firebase (Test)"
  lane :dsym_test do
    upload_symbols_to_crashlytics(
      dsym_path: "./artifact/automateDeploy.app.dSYM.zip", //path ของ dsym ที่เราสร้างได้จาก script gym ด้านบน
      gsp_path: "./Firebase/GoogleService-Info.plist", //path ของ google service ที่อยู่ในโปรเจคเรา
      debug: true // เปิดใช้งานโหมดดีบักสำหรับการอัพโหลด dSYM 
      )
  end
```

สามารถดู parameter ต่างๆ ที่ใช้ได้ภายใต้ upload_symbols_to_crashlytics ที่ลิ้งค์นี้นะจ๊ะ https://docs.fastlane.tools/actions/upload_symbols_to_crashlytics/

เขียน Script เสร็จก็ลอง Run กันนน
```sh
fastlane dsym_test // dsym_test คือขื่อ Lane ของด้านบน
```

# Run หลาย Lane ?
เราสามารถใช้หลายๆ lane พร้อมกันได้ด้วยนะ แค่เราพิมพ์ && ระหว่างชื่อ lane ประมาณนี้นะจ๊ะ 

เช่น คำสั่งนี้เราจะ build app พร้อมกับปล่อยให้ User test ไปเล้ย และ อัพโหลด dsym ภายในเวลาเดียวกัน

```sh
fastlane build_test && fastlane distribute_test && fastlane dsym_test
```

<!-- LICENSE -->
## License

Nipa Chamyuang



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Document Fastlane](https://docs.fastlane.tools)
* [Firebase App Distribution](https://firebase.google.com/docs/app-distribution/ios/distribute-fastlane)






<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[images-fastlane]: images/fastlane.png
[images-install-fastlane]: images/install-fastlane.png
[images-install-fastlane-file]: images/install-fastlane-file.png
[images-gym-build]: images/gym-build.png
[images-firebase-login]: images/firebase-login.png

